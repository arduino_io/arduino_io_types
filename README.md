# arduino_io_types

This package is intended to help creating servers or mobile app for [arduino_io](https://www.arduino.cc/reference/en/libraries/simple-repository-io/)

Here you'll find the types compatible with the library above.
