info:    # show the make options
	@clear
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z_\-]*: *.*"

login:   # logs in to your npm account
	@npm login

whoami:  # checks who is logged in
	@npm whoami

build:   # builds this package
	@npm run build

publish: # builds and publish this package
	@make build
	@npm publish --access=public
