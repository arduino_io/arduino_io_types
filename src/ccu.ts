import { Infer, number, object, string } from "superstruct"
import Hub from "./hub"

const CCU = object({
	ip: string(),
	port: number(),
	hub: Hub,
})

type CcuType = Infer<typeof CCU>

export default CCU
export type { CcuType }
