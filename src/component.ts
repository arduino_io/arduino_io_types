import { Infer, boolean, enums, number, object, string, union } from "superstruct"

enum IoType {
	IN = "input",
	OUT = "output",
}

enum DataType {
	BOOLEAN = "boolean",
	PERCENT = "percent",
}

const Value = union([number(), boolean()])

const Component = object({
	name: string(),
	io: enums([IoType.IN, IoType.OUT]),
	data: enums([DataType.BOOLEAN, DataType.PERCENT]),
	value: Value,
})

type ComponentType = Infer<typeof Component>

export default Component
export { IoType, DataType }
export type { ComponentType }
