import { Infer, array, object, string } from "superstruct"
import Component from "./component"

const Hub = object({
	name: string(),
	items: array(Component),
})

type HubType = Infer<typeof Hub>

export default Hub
export type { HubType }
